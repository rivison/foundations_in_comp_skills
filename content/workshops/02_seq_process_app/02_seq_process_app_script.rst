Workshop 2 scripts
==================

Introduction
------------

In this workshop we will use the skills you learned in the command line basics
and python workshops to process real-world high-throughput sequencing data.

These pre-materials will introduce all the concepts you need to immediately
jump in and start work on the workshop materials.

This project uses genome-wide high-througput RNA sequencing, so before we cover
the actual experiment we will cover the basics of sequencing.

The first video in these materials is an animation that describes Illumina
short read sequencing technology, which is currently the most popular form of
high-throughput sequencing.

.. note::
    https://www.youtube.com/watch?v=fCd6B5HRaZ8

The analysis portion of the first video describes genomic DNA sequencing, so we
created a second video that describes the data produced by the Illumina sequencer
and a general description of how to analyze the data.

If you are already comfortable with this type of data, feel free to skip
forward to the Problem Description section.

High-Throughput Sequencing Data
-------------------------------

As of 2017, it is very common for researchers to supply input material, usually
in the form of extracted DNA or RNA from some system of interest, to
organizations called "sequencing centers" or "sequencing core facilities" that
own and operate sequencing machines.

The actual process of sequencing as described in Illumina's videos is therefore
seldom actually performed by researchers themselves, unless significant
customization to the process is necessary for their problem of interest.

High-throughput sequencing data is most often provided to researchers, and
made available through sequencing dataset repositories, in the FASTQ file
format.

.. note::
    switch to fastq wikipedia page

The FASTQ format is a text-based format that contains sequences identified by
the sequencer, and is organized into sets of individual sequences, called
reads.

One read is represented by four lines in the FASTQ file:

   - the header, which starts with an @ symbol and contains information about
     the machine that produced the sequencing, the flowcell, the location of
     the spot on the flowlane, and any barcode information, which we don't
     cover here.
   - the sequence of the read, which always uses the DNA alphabet of A, C, G,
     and T regardless of whether the input material is DNA or RNA
   - an additional header starting with a + that is usuall blank
   - a sequence of characters, which is also called a string, that is the same
     length as the sequence read and encodes quality scores for each base

Each character in the quality score string is matched to the nucleotide
character in corresponding DNA sequence, so the first score character gives us
information about the first sequenced base, the fifth score character the fifth
sequenced base, etc.

.. note::
    walk through this by highlighting the bases on the wiki page

The quality scores, also called PHRED scores, translate to a number that
corresponds to the confidence the base call made by the sequencing machine is
what is reported it to be.

I won't go into exactly how this score is calculated, or how it is encoded in
FASTQ format; all that you need to know is that each base in a read has an
associated quality that can be used later for quality control.

There are presently two types of read datasets produced by Illumina sequencers:
single end and paired end.

As previously mentioned, the input to sequencing is usually short DNA fragments.

Single end reads capture one end of each fragment, meaning each fragment has
only one read in the resulting dataset, stored in a single FASTQ formatted file.

Paired end reads capture both ends of each fragment, meaning each fragment has
two reads in the resulting dataset, usually stored in a pair of FASTQ formatted
files.

Any input material can be used to make either single or paired end data, and
the choice depends on the question being asked.

Typically, paired end sequencing is only used when sequencing mRNA, for reasons
that will hopefully become clear later.

Most other types of sequencing experiments only need single end reads.

Besides single vs paired end, another important property of a sequencing data
is the read length.

In the FASTQ files that came directly from a sequencer, all of the reads
will be exactly the same length.

This is due to the way the sequencing machine works when it encorporates bases
in the sequence synthesis process.

The sequence length is a parameter that is specified by the researcher, or the
core producing the data, at the time the sequencing is performed.

Obviously, the longer each read is, the more sequenced bases a dataset
contains, and the more information is (likely) contained in the dataset.

For most sequencing applications, a question of interest is: where in a genome
did a given read originate?

To do this, a read must be mapped back to a reference sequence, for example,
the human genome.

In general, longer reads, on account of containing more information, can be
mapped more confidently back to a reference sequence.

It is this consideration that often motivates the choice of read length; namely
what is the complexity of the genome being targeted, and therefore how
confident must we be in our read mapping.

For the purposes of mapping, longer reads are generally better, but very long
reads may be overkill for some purposes.

A full discussion of this topic is beyond the scope of this video, but suffice
it to say that the two properties of a dataset you should pay attention to are
read length and whether it is single or paired end.


Sequencing quality control and analysis basics
----------------------------------------------

Once you have FASTQ files, the first task is to assess the quality of the
reads.

The first quality control metric that is of interest is the average quality
score per base across all of the reads in the dataset.

.. note::
    switch to fastqc example output

One program very often used in sequence data QC is fastqc, which computes a
number of metrics using the reads in a FASTQ file.

If the reads you find in a FASTQ file are not all the same length, you know that
some post-processing has already been done.

The most common form of post processing is called 'adapter clipping', where
bases that correspond to the sequencing adapter molecules introduced by the
sequencing protocol are included in the sequenced read.

These adapter sequences must be removed from the reads, if they exist, since
the adapter sequence was designed such that it does not appear in any known
genome and are therefore of no interest to the researcher.

In addition to clipping adapters, it is also common to trim off the end of
reads that have poor quality bases.

In this fastqc output, you can see that the average base quality degrades as
the base position in the read increases.

This is normal, but some reads may have bases of unacceptable at the end that
may affect downstream analysis.

To avoid this, low quality bases at the ends of reads can be removed, thus
shortening the read but maintaining higher overall read quality.

As a result of trimming, the reads are no longer all the same length, and some
reads may be excessively trimmed, such that the surviving high quality bases
make a read that is too short for reliable mapping.

For genomic sequences, a minimum length of 35 is often used, and any reads that
become shorter due to adapter or quality trimming are removed from the dataset.

For paired end sequencing, it is common that if one end of a read pair is
filtered for length, both ends are filtered.

There are a number of published programs that perform this adapter and quality
clipping, including fastx-toolkit, trimmomatic, cutadapt, and others.

These tools are all similar, but perform essentially the same function.

There are other interesting metrics that can be calculated from a FASTQ
dataset, including GC content, overrepresented sequences, etc but for the
sake of brevity they are not covered in detail in this video.

High quality reads are used for downstream analysis, which greatly depends on
the question of interest.

With the exception of ab initio genome or transcriptome assembly efforts,
nearly all sequencing datasets are intended to be mapped to a reference
sequence, like the human genome, so that will be our focus for the rest of this
video.

The practical effect of filtering out poor quality reads is that it enables us
to obtain the highest mapping quality possible.

Mapping quality is important, because the entire meaning of a dataset is tied
to examining where, and in some cases how well, a dataset maps to a genome of
interest.

Mapping reads to a genome is essentially the process of finding one, short
string inside another, long string.

There are many different strategies, and even more programs, available to do
this, but the important thing to know about mapping is that, ultimately,
reads that encode sequences originating from some part of the genome are
assigned the appropriate genomic position, or positions, from whence it came.

Many factors affect this, including sequencing quality and genome complexity,
and a full treatment of mappability is well beyond the scope of this video.

The important concept to remember for mapping reads to a genome is that the
locations where reads map, and the proportion of reads that map to particular
locations, is the fundamental output of many sequencing experiments, including
those we will focus on in these workshops.


Problem Description
-------------------

A researcher at BU is interested in epitranscriptomics.

Epitranscriptomics refers to the study of post-transcriptional biochemical
modifications to RNA, similar to the concept of epigenomics with DNA, where
modifications to DNA nucleotides, rather than the bases themselves, are modified.

.. note::
    switch to figure epitranscriptomic_marks.png

Epitranscriptomics manifest in the form of *marks*, or single chemical
modifications to individual residues that affect how the RNA species is
processed downstream.

In this figure, six such marks are depicted, but there are more than 150 such
marks known.

Unlike epigenomic marks, which have been successfully determined genome-wide
using chromatin immunoprecipitation plus high throughput sequencing techniques,
identifying epitranscriptomic marks genome-wide is relatively much more
difficult.

.. note::
    switch to figure epitranscriptomic_methods.png

A number of methods have been developed to identify epitranscriptomic marks
using high-throughput sequencing data, some examples are illustrated here.

For more information, you can check out this review:

   Helm, Mark, and Yuri Motorin. 2017. “Detecting RNA Modifications in the
   Epitranscriptome: Predict and Validate.” Nature Reviews. Genetics 18 (5):
   275–91.

.. note::
    switch to https://en.wikipedia.org/wiki/2%27-O-methylation

The BU researcher is working with a sequencing method called 2OMeSeq, which
seeks to identify RNA residues that have the 2'-O-methylation modification.

.. note::
    switch to https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5388417/figure/F1/

2OMeSeq was developed following the observation that, under low dNTP
conditions, reverse transcription stalls at 2'-OMe sites but has complete
read-through otherwise.

.. note::
    switch to https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5388417/figure/F2/

As a result, when the RNA fragments are size selected and sequenced, there is
a larger proportion of reads that map immediately adjacent to 2'OMe sites in
the low dNTP conditions than normal, as seen in the figure.

However, since the number of 2OMe sites in the transcriptome are relatively few
when compared to the size of the genome, the source RNA concentration is small,
and must undergo several rounds of PCR prior to sequencing.

We therefore would like to be able to differentiate between PCR amplification
products and distinct 2OMe events.

To do so, the researcher employed a custom primer strategy, such that each RNA
fragment in the input sent for sequencing was as follows::

   4 random nucleotides
   |
   |   True RNA fragment insert
   |               |
   |               |            2 random nucleotides
   |               |            |
   |               |            | Literal CACA
   |               |            | |
   v               v            v v
   NNNNXXXXXXXXXXXXXXXXXXXXXXXXXNNCACA

Reads that end in the ``CACA`` sequence are RNA fragments that were
successfully subjected to the custom adapter protocol, and thus reads that do
not end with ``CACA`` should be discarded.

Of the reads that remain, the random nucleotide sequences in combination with
the true RNA insert, allow us to assume that any reads with duplicate sequence
are the result of PCR amplification and therefore only represent a single true
modification event.

In this workshop, we will use linux and python to process reads from this real
experiment in order to:

1. Identify reads that were successfully ligated (i.e. end with ``CACA``)
2. De-duplicate the reads so that each is unique
3. Trim off the beginning and trailing random sequences from the reads

Using this randomization scheme, any reads in the resulting data correspond to
unique RNA species and can therefore be confidently studied for evidence of
true 2OMe events by mapping to the genome and counting 5' events.
