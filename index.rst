.. Foundations in Computational Skills documentation master file, created by
   sphinx-quickstart on Mon Jul 10 16:33:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Foundations in Computational Skills's documentation!
===============================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   content/workshops/00_cli_basics/00_cli_basics
   
   content/workshops/02_seq_process_app/02_seq_process_app

   scripts
   
List of topics
--------------

:doc:`content/workshops/00_cli_basics/00_cli_basics`

======================================== =======
Topic                                    Length
======================================== =======
`Introduction`_                          TODO
`Navigating directories, listing files`_ 16 min
`Basic file operations`_                 9 min
`Working with files 1`_                  10 min
`Working with files 2`_                  10 min
`I/O redirection and related commands`_  20 min
`Globbing`_                              TODO
======================================== =======

.. _`Introduction`: http://www.youtube.com
.. _`Navigating directories, listing files`: https://www.youtube.com/watch?v=MmHcOPJEjGA
.. _`Basic file operations`: https://www.youtube.com/watch?v=qG8qn4ZARvg
.. _`Working with files 1`: https://www.youtube.com/watch?v=hNb8gIHvN04
.. _`Working with files 2`: https://www.youtube.com/watch?v=MJI4xTuxdPg
.. _`I/O redirection and related commands`: https://www.youtube.com/watch?v=WXcvQ5F7Kh4
.. _`Globbing`: https://www.youtube.com

:doc:`content/workshops/01_python/01_python`

TODO

:doc:`content/workshops/02_seq_process_app/02_seq_process_app`

========================================= =======
Topic                                     Length
========================================= =======
`Introduction`_                           TODO
`Illumina Sequencing Technology`_         5 min
`High Throughput Sequencing Data Primer`_ TODO
`Sequencing Data QC and Analysis Primer`_ TODO
========================================= =======

.. _`Introduction`: http://www.youtube.com
.. _`Illumina Sequencing Technology`: https://www.youtube.com/watch?v=fCd6B5HRaZ8
.. _`High Throughput Sequencing Data Primer`: http://www.youtube.com
.. _`Sequencing Data QC and Analysis Primer`: http://www.youtube.com

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
